# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from pylab import *

import time

import timeit

#print("Hello World!")

#print(cos(2))

t = time.clock()

print(t)

a = array([4,6,5,8])

#print("This is vector a:",a)

#print(a[2])

#print(len(a))

b = linspace(1,30,6)

#print(b)

c = arange(0,10,2)

print(c)

d = arange(4,9)

print(d)

print(dot(c,d))

def f(x):
    return(cos(x**2))

print(f(5))

print("Time difference",(time.clock()-t), "s")

x=100


T = timeit.Timer('f(x)', 'from __main__ import f,x')
print('10 000 Evaluations take ', T.timeit(number=10000), 's')