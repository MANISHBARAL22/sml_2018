# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from pylab import *


C = arange(1,11)

print(len(C))

print(C.size)

print(C)

C1 = C[::-1]

C2 = C[3:9:2]

print(C1)

print(C2)

D = array([3,4])

print(r_[C,D])

print(polyval(D,2))  # 3 * 2**1 + 4 

print(polyval(D[::-1],2))  # 4 * 2**1 + 3

def f(x):
    return x**2-4

print(brentq(f,0,100))

I = 55.5436546364

print('{I:10.4f}'.format(**locals()))

print(round(I,2))